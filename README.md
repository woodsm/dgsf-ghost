# dgsf-ghost

![Uptime Robot status](https://img.shields.io/uptimerobot/status/m785897373-5da7a6fb61156440fc5b7558?label=%205min%20Status)
![Uptime Robot ratio (7 days)](https://img.shields.io/uptimerobot/ratio/7/m785897373-5da7a6fb61156440fc5b7558?label=7%20Day%20Uptime)
![Uptime Robot ratio (30 days)](https://img.shields.io/uptimerobot/ratio/m785897373-5da7a6fb61156440fc5b7558?label=30%20Day%20Uptime)
![Node.js CI](https://github.com/dreamgirlfishing/dgsf-ghost/workflows/Node.js%20CI/badge.svg)
![Website](https://img.shields.io/website?url=https%3A%2F%2Fdreamgirlfishing.com)
![GitHub language count](https://img.shields.io/github/languages/count/dreamgirlfishing/dgsf-ghost)
![GitHub top language](https://img.shields.io/github/languages/top/dreamgirlfishing/dgsf-ghost)

## Ghost Theme Overview

Ghost uses a simple templating language called [Handlebars](http://handlebarsjs.com/) for its themes.

This theme has lots of code comments to help explain what's going on just by reading the code. Once you feel comfortable with how everything works, we also have full [theme API documentation](https://ghost.org/docs/api/handlebars-themes/) which explains every possible Handlebars helper and template.

**The main files are:**

- `default.hbs` - The parent template file, which includes your global header/footer
- `index.hbs` - The main template to generate a list of posts, usually the home page
- `post.hbs` - The template used to render individual posts
- `page.hbs` - Used for individual pages
- `tag.hbs` - Used for tag archives, eg. "all posts tagged with `news`"
- `author.hbs` - Used for author archives, eg. "all posts written by Jamie"

One neat trick is that you can also create custom one-off templates by adding the slug of a page to a template file. For example:

- `page-about.hbs` - Custom template for an `/about/` page
- `tag-news.hbs` - Custom template for `/tag/news/` archive
- `author-ali.hbs` - Custom template for `/author/ali/` archive

## Development

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/mxttwoods/dgsf-ghost-hb)

Styles are compiled using Gulp/PostCSS to polyfill future CSS spec. You'll need [Node](https://nodejs.org/), [Yarn](https://yarnpkg.com/) and [Gulp](https://gulpjs.com) installed globally. After that, from the theme's root directory:

```bash
# install dependencies
yarn install

# run development server
yarn dev
```

Now you can edit `/assets/css/` files, which will be compiled to `/assets/built/` automatically.

The `zip` Gulp task packages the theme files into `dist/<theme-name>.zip`, which you can then upload to your site.

```bash
# create .zip file
yarn zip
```

## PostCSS Features Used

- Autoprefixer - Don't worry about writing browser prefixes of any kind, it's all done automatically with support for the latest 2 major versions of every browser.
- Variables - Simple pure CSS variables
- [Color Function](https://github.com/postcss/postcss-color-function)

## SVG Icons

This project uses inline SVG icons, included via Handlebars partials. You can find all icons inside `/partials/icons`. To use an icon just include the name of the relevant file, eg. To include the SVG icon in `/partials/icons/rss.hbs` - use `{{> "icons/rss"}}`.

You can add your own SVG icons in the same manner.

## Copyright & License

Copyright (c) 2020 Matthew Woods - Released under the [MIT license](LICENSE).
