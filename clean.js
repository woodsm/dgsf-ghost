// clean.js

var fs = require('fs');

function deleteZipRecursive(path) {
    if (fs.existsSync(path) && fs.lstatSync(path).isDirectory()) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + '/' + file;

            if (fs.lstatSync(curPath).isDirectory()) {
                // recurse
                deleteZipRecursive(curPath);
            } else {
                // delete file
                fs.unlinkSync(curPath);
            }
        });

        console.log(`Deleting directory "${path}"...`);
        fs.rmdirSync(path);
    }
}

function deleteDistRecursive(path) {
    if (fs.existsSync(path) && fs.lstatSync(path).isDirectory()) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + '/' + file;

            if (fs.lstatSync(curPath).isDirectory()) {
                // recurse
                deleteDistRecursive(curPath);
            } else {
                // delete file
                fs.unlinkSync(curPath);
            }
        });

        console.log(`Deleting directory "${path}"...`);
        fs.rmdirSync(path);
    }
}

// function deleteNodeRecursive(path) {
// 	if (fs.existsSync(path) && fs.lstatSync(path).isDirectory()) {
// 		fs.readdirSync(path).forEach(function (file, index) {
// 			var curPath = path + '/' + file;

// 			if (fs.lstatSync(curPath).isDirectory()) {
// 				// recurse
// 				deleteNodeRecursive(curPath);
// 			} else {
// 				// delete file
// 				fs.unlinkSync(curPath);
// 			}
// 		});

// 		console.log(`Deleting directory "${path}"...`);
// 		fs.rmdirSync(path);
// 	}
// }

// function deleteReportAsync(path) {
//   try {
//     fs.unlinkSync(path);
//     console.log(`Deleting file "report.json"...`);
//   } catch (err) {
//     console.log(`No "report.json" found`);
//   }
// }

// console.log("Cleaning Results and Misc. files");
deleteDistRecursive('./assets/built');
deleteZipRecursive('./dist');
// deleteNodeRecursive('./node_modules');
// deleteReportAsync("./report.json");
console.log('Successfully cleaned working tree!');
